import { Injectable, EventEmitter } from "@angular/core";
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthService {
  private loggedIn : boolean = false;
  private _at : string = null;
  private franchiseName : string = null;
  private tab : number = 0;
  private subscription : EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private storage: Storage) {
    this.storage.get("_ffn").then(ffn => {
      this.franchiseName = ffn;
      return storage.get("_fat")
    }).then(at => {
      this._at = at;
      storage.get("_t").then(tab => {
          this.tab = tab;
      })
      if (this.franchiseName && this._at) {
        this.loggedIn = true;
      }
    });
  }

  // SETTERS
  public authenticate(body : any, cb : Function) {
    this.franchiseName = body._ffn;
    this._at = body._tk;
    this.tab = 0;
    this.storage.set("_ffn", this.franchiseName);
    this.storage.set("_fat", this._at);
    this.loggedIn = true;
    cb();
  }

  public logout() {
    this.loggedIn = false;
    this.franchiseName = null;
    this._at = null;
    this.storage.remove("_ffn").then(() => this.storage.remove("_fat")).then(() => {
      this.subscription.emit(true);
    });
  }

  public setTab(tab : number) {
      this.tab = tab;
      this.storage.set("_bp", this.tab);
  }

  public addTab(tab : number) {
      this.tab += tab;
      this.storage.set("_bp", this.tab);
  }

  // GETTERS

  public isAuthenticated() {
    return new Promise((resolve, reject) => {
      this.storage.get("_ffn").then(ffn => {
        this.franchiseName = ffn;
        return this.storage.get("_fat")
      }).then(at => {
        this._at = at;
        if (this.franchiseName && this._at) {
          this.loggedIn = true;
        }
        resolve(this.loggedIn);
      });
    });
  }

  public getSubcription() {
    return this.subscription;
  }

  public getFranchiseName() {
    return this.franchiseName;
  }

    public getTab() {
        return this.tab;
    }

  public getAUTH() {
    return this._at;
  }
}
