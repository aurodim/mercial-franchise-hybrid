import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/retry';
import { SnackbarService } from "../snackbar/snackbar.service";
import { LoaderService } from "../loader/loader.service";
import { catchError } from 'rxjs/operators';

@Injectable()
export class APIService {
  public api : string;

  constructor(private http : HttpClient, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService) {
    this.api = process.env.BASE_URL;
  }

  AUTH() {
    return this.authService.getAUTH();
  }

  // GET

  analyticsData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/analytics", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : any) => this.ErrorHandler(error));
  }

  helpData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/help", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  locationsData(onError) : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/locations", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  offersAnalyticsData(id : string) : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/offers/analytics", { params : { 'auth' : this.authService.getAUTH(), 'offer' : id}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  offersData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/offers", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  editorData(id : string, onError) : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/offers/edit", { params : { 'auth' : this.authService.getAUTH(), 'offer' : id}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error, onError));
  }

  redeemsData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/redeems", { params : { 'auth' : this.authService.getAUTH() }, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  settingsData() : Observable<Object> {
    return this.http
    .get(this.api + "/api/franchise/settings", { params : { 'auth' : this.authService.getAUTH()}, responseType : 'json' })
    .retry(3)
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  // POST

  register(requirements : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(requirements);
    return this.http.post(this.api + "/api/franchise/register", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  login(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/franchise/login", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  forgot(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/franchise/forgot", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  reset(credentials : object) {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(credentials);
    return this.http.post(this.api + "/api/franchise/reset", body, {"headers" : headers})
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  deleteOffer(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/offers/delete", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateOffer(values : any) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/offers/edit?id="+values.id, body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateAccount(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/account", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateFranchise(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/franchise", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateLegal(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/legal", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateLocation(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/location", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateManagers(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/managers", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updateCC(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/cc", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  redeemTab(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/redeem", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  updatePrivacy(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/settings/privacy", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  deleteAccount(values : object) : Observable<Object> {
    const headers = new HttpHeaders({'Content-Type' : 'application/json'});
    const body = JSON.stringify(values);
    return this.http.post(this.api + "/api/franchise/delete", body, { headers : headers })
    .catch((error : HttpErrorResponse) => this.ErrorHandler(error));
  }

  ErrorHandler(error : HttpErrorResponse, onError? : Function) {
    this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
    if (error.status === 401) {
      this.authService.logout();
      return Observable.empty<null>();
    }
    if (onError) onError();
    this.loaderService.handleLoader(false);
    return Observable.empty<null>();
  }
}
