import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { MercialFranchises } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts';
import { ThreeDeeTouch } from '@ionic-native/three-dee-touch';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { Globalization } from '@ionic-native/globalization';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { NoAuthComponent } from '../pages/NoAuth/noauth.component';
import { AuthComponent } from '../pages/Auth/auth.component';
import { LandingComponent } from '../pages/NoAuth/landing/landing.component';
import { LoaderModule } from './loader/loader.module';
import { DatePicker } from '@ionic-native/date-picker';
import { SnackbarService } from './snackbar/snackbar.service';
import { AuthService } from './auth/auth.service';
import { DashboardComponent } from '../pages/Auth/dashboard/dashboard.component';
import { HelpComponent } from '../pages/Auth/help/help.component';
import { SettingsComponent } from '../pages/Auth/settings/settings.component';
import { LoginComponent } from '../pages/NoAuth/login/login.component';
import { RegisterComponent } from '../pages/NoAuth/register/register.component';
import { ForgotPasswordComponent } from '../pages/NoAuth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '../pages/NoAuth/reset-password/reset-password.component';
import { APIService } from './auth/api.service';
import { RedeemsComponent } from '../pages/Auth/redeems/redeems.component';
import { OffersComponent } from '../pages/Auth/offers/offers.component';
import { Keyboard } from '@ionic-native/keyboard';
import { EditOfferComponent } from '../pages/Auth/offers/edit/edit-offer.component';
import { NewOfferComponent } from '../pages/Auth/offers/new/new-offer.component';
import { OfferComponent } from '../pages/Auth/offers/offer/offer.component';
import { FranchiseOfferService } from '../pages/Auth/offers/offers.service';
import { ScannerComponent } from '../pages/Auth/scanner/scanner.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AutoCompleteLocationComponent } from '../pages/shared/autocomplete-location.component';
import { Pro } from '@ionic-native/pro';

import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Network } from '@ionic-native/network';
import { MessengerComponent } from '../pages/Auth/help/messenger/messenger.component';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { Stripe } from '@ionic-native/stripe';
import { ManagerComponent } from '../pages/NoAuth/manager/manager.component';
import { OfferAnalyticsComponent } from '../pages/Auth/offers/analytics/analytics-offer.component';
import { LegalViewerComponent } from '../pages/Legal/legal.viewer.component';
import { HelperViewerService } from '../pages/Auth/help/viewer/viewer.helper.service';
import { HelpViewerComponent } from '../pages/Auth/help/viewer/viewer.helper.component';
import { LoaderService } from './loader/loader.service';
import { isDevMode } from '@angular/core';

const config: SocketIoConfig = { url: process.env.BASE_URL ? process.env.BASE_URL : 'https://www.mercial.io', options: {autoConnect : false, secure: true, path: '/io/listener/'} };
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, `https://res.cloudinary.com/aurodim/raw/upload/v${process.env.VERSION}/mercial/static/translations/`, ".json");
}

const GoogleMapsCore = AgmCoreModule.forRoot({
  apiKey : 'AIzaSyDS7onFHWBlfNDdcCRt6rOwKepdoZKR67A',
  libraries: ['places']
});

@NgModule({
  declarations: [
    MercialFranchises,
    NoAuthComponent,
    AuthComponent,
    LegalViewerComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    DashboardComponent,
    OffersComponent,
    EditOfferComponent,
    NewOfferComponent,
    OfferAnalyticsComponent,
    OfferComponent,
    RedeemsComponent,
    HelpComponent,
    MessengerComponent,
    HelpViewerComponent,
    ScannerComponent,
    SettingsComponent,
    ManagerComponent,
    AutoCompleteLocationComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MercialFranchises,
      {
        autocomplete: 'on',
        platforms : {
          ios : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
          },
          android : {
            // These options are available in ionic-angular@2.0.0-beta.2 and up.
            scrollAssist: false,    // Valid options appear to be [true, false]
            autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
          }
          // http://ionicframework.com/docs/v2/api/config/Config/)
        }
      }
    ),
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'aurodim' } as CloudinaryConfiguration),
    IonicStorageModule.forRoot({
      name: '__merbizdb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    SocketIoModule.forRoot(config),
    TranslateModule.forRoot({
       loader: {
           provide: TranslateLoader,
           useFactory: HttpLoaderFactory,
           deps: [HttpClient]
       }
    }),
    HttpClientModule,
    LoaderModule,
    ChartsModule,
    GoogleMapsCore
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MercialFranchises,
    AuthComponent,
    LegalViewerComponent,
    NoAuthComponent,
    LandingComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    DashboardComponent,
    OffersComponent,
    EditOfferComponent,
    OfferAnalyticsComponent,
    NewOfferComponent,
    HelpViewerComponent,
    OfferComponent,
    RedeemsComponent,
    HelpComponent,
    MessengerComponent,
    ScannerComponent,
    SettingsComponent,
    ManagerComponent,
    AutoCompleteLocationComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    Camera,
    Globalization,
    FileTransfer,
    BarcodeScanner,
    DatePicker,
    FileTransferObject,
    FileTransfer,
    File,
    APIService,
    AuthService,
    SnackbarService,
    Network,
    Stripe,
    Pro,
    InAppBrowser,
    LoaderService,
    ThreeDeeTouch,
    HelperViewerService,
    FranchiseOfferService,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
