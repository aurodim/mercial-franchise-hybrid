import { Component, NgZone, ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'autocomplete-location',
  templateUrl: 'autocomplete-location.component.html'
})
export class AutoCompleteLocationComponent {
  @ViewChild('address') public addressElem: any;

  constructor(private mapsAPILoader: MapsAPILoader, private params: NavParams, private ngZone: NgZone, private viewController : ViewController) {}

  ionViewDidLoad() {
    this.mapsAPILoader.load().then(
      () => {
        if (!this.addressElem) {
          return;
        }
        let types = this.params.get("type") ? this.params.get("type") : [];
       let autocomplete = new google.maps.places.Autocomplete(this.addressElem._searchbarInput.nativeElement, { types: types });
        autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
           let place: google.maps.places.PlaceResult = autocomplete.getPlace();
           if(place.geometry === undefined || place.geometry === null ){
            return;
           }

           this.viewController.dismiss(place);
          });
        });
      }
    );
  }
}
