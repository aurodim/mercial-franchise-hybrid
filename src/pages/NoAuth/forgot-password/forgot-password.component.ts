import { Component, ViewChild } from "@angular/core";
import { LoginComponent } from "../login/login.component";
import { ResetPasswordComponent } from "../reset-password/reset-password.component";
import { TextInput, App, Nav } from "ionic-angular";
import { NgForm } from "@angular/forms";
import { LoaderService } from "../../../app/loader/loader.service";
import { APIService } from "../../../app/auth/api.service";
import { AuthService } from "../../../app/auth/auth.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: 'forgot-password.component.html'
})
export class ForgotPasswordComponent {
  login = LoginComponent;
  reset = ResetPasswordComponent;
  @ViewChild('dateInput') dateInput : TextInput;

  constructor(private app : App, private nav : Nav, private loaderService : LoaderService, private api : APIService, private authService : AuthService, private snackbarService : SnackbarService) {}

  onSubmit(forgotForm : NgForm) {
    if (forgotForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.forgot(forgotForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_mismatch") {
          forgotForm.controls['email'].setErrors({'invalid' : true});
          forgotForm.controls['franchise'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "email_franchise_mismatch", "isError" : true});
        } else {
          this.snackbarService.handleError({"message" : "reset_pass_email_success", "isError" : false});
          forgotForm.resetForm();
          this.nav.push(this.reset);
        }
        this.loaderService.handleLoader(false);
      },
      error => {
        this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
        this.loaderService.handleLoader(false);
      }
    );
  }

  ionViewWillEnter() {
     this.nav.swipeBackEnabled = false;
  }

  ionViewWillLeave() {
      this.nav.swipeBackEnabled = true;
  }
}
