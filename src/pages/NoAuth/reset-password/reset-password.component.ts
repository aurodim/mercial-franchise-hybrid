import { Component } from "@angular/core";
import { LoginComponent } from "../login/login.component";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../../app/auth/auth.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { NavParams, Nav } from "ionic-angular";
import { APIService } from "../../../app/auth/api.service";

@Component({
  selector: 'app-reset-password',
  templateUrl: 'reset-password.component.html'
})
export class ResetPasswordComponent {
  login = LoginComponent;
  forgot = ForgotPasswordComponent;

  resetForm = new FormGroup({
    code : new FormControl('', { validators: [Validators.required]}),
    email : new FormControl('', { validators: [Validators.email, Validators.required]}),
    "new-password" : new FormControl('', { validators: [Validators.required]}),
    "confirm-password" : new FormControl('', { validators: [Validators.required]})
  });

  constructor(private navParams : NavParams, private nav : Nav, private api : APIService, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService) {}

  ngOnInit() {
    if (this.navParams.get("code")) {
      this.resetForm.controls.code.setValue(this.navParams.get("code"));
      if (this.navParams.get("code").length < 6 || this.navParams.get("code").length > 8) {
        this.resetForm.controls.code.markAsDirty();
      }
    }

    if (this.navParams.get("email")) {
      this.resetForm.controls.email.setValue(this.navParams.get("email"));
    }
  }

  mismatch() {
    if (this.resetForm.controls["new-password"].value !== this.resetForm.controls["confirm-password"].value) {
      this.resetForm.controls["confirm-password"].setErrors({mismatch : true});
    }
  }

  onSubmit() {
    if (this.resetForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.reset(this.resetForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_passshort") {
          this.resetForm.controls['new-password'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "pass_len_error", "isError" : true});
        } else if (response._body._m === "_invalidcode") {
          this.resetForm.controls['email'].setErrors({'invalid' : true});
          this.resetForm.controls['code'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "email_code_mismatch", "isError" : true});
        } else {
          this.snackbarService.handleError({"message" : "pass_reset_success", "isError" : false});
          this.resetForm.reset();
        }
        this.loaderService.handleLoader(false);
      },
      error => {
        this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
        this.loaderService.handleLoader(false);
      }
    );
  }

  ionViewWillEnter() {
     this.nav.swipeBackEnabled = false;
  }

  ionViewWillLeave() {
      this.nav.swipeBackEnabled = true;
  }
}
