/// <reference types="@types/googlemaps" />
import { Component, ViewChild } from "@angular/core";
import { LoginComponent } from "../login/login.component";
import { Nav, App, ModalController } from "ionic-angular";
import { APIService } from "../../../app/auth/api.service";
import { AuthService } from "../../../app/auth/auth.service";
import { AuthComponent } from "../../Auth/auth.component";
import { NgForm } from "@angular/forms";
import {} from "googlemaps";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { AutoCompleteLocationComponent } from "../../shared/autocomplete-location.component";
import { LegalViewerComponent } from "../../Legal/legal.viewer.component";

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html'
})
export class RegisterComponent {
  login = LoginComponent;
  @ViewChild('registerForm') registerForm : NgForm;
  franchise_place = null;

  constructor(private app : App, private modalController : ModalController, private nav:Nav, private loaderService : LoaderService, private api : APIService, private authService : AuthService, private snackbarService : SnackbarService) {}

  ionViewDidLoad() {
    document.getElementsByClassName('legal-tos')[0].addEventListener('click', () => {
      this.onLegalView('terms-of-use');
    });
    document.getElementsByClassName('legal-pp')[0].addEventListener('click', () => {
      this.onLegalView('privacy-policy');
    });
    document.getElementsByClassName('legal-dis')[0].addEventListener('click', () => {
      this.onLegalView('disclaimer')
    });
  }

  onLegalView(doc) {
    this.app.getRootNav().push(LegalViewerComponent, {type: doc});
  }

  resetLocation() {
    this.franchise_place = null;
    this.registerForm.controls["franchise-lat"].setValue(null);
    this.registerForm.controls["franchise-lng"].setValue(null);
    this.registerForm.controls["franchise-postal"].setValue(null);
    this.registerForm.controls["franchise-address"].setValue(null);
    this.registerForm.controls["franchise-address"].setErrors({"invalidaddress" : true});
    this.startAutocomplete();
  }

  startAutocomplete() {
    let autoCompleteModal = this.modalController.create(AutoCompleteLocationComponent);
    autoCompleteModal.onDidDismiss(place => {
      if (!place) return;
      this.registerForm.controls["franchise-address"].setErrors(null);
      this.registerForm.controls["franchise-address"].setValue(place.formatted_address);
      this.registerForm.controls["franchise-lat"].setValue(place.geometry.location.lat());
      this.registerForm.controls["franchise-lng"].setValue(place.geometry.location.lng());
      place.address_components.forEach((component) => {
         if (component.types.includes("country")) {
           this.registerForm.controls["franchise-country"].setValue(component.short_name);
         }
      });
      this.registerForm.controls["franchise-postal"].setValue(place.address_components.filter(component => component.types.includes("postal_code"))[0].long_name);
      this.franchise_place = place;
    });
    autoCompleteModal.present();
  }

  onRegisterSubmit() {
    if (this.registerForm.invalid) {
      return;
    }
    if (!this.franchise_place || this.franchise_place === null) {
      this.registerForm.controls["franchise-address"].setErrors({"invalidaddress" : true});
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.register(this.registerForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_emailinuse") {
          this.registerForm.controls['email'].setErrors({'inuse' : true});
          this.snackbarService.handleError({"message" : "email_inuse", "isError" : true});
        } else if (response._body._m === "_passshort") {
          this.registerForm.controls['password'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "pass_len_error", "isError" : true});
        } else if (response._body._m === "_fradr") {
          this.registerForm.controls['franchise-name'].setErrors({'invalid' : true});
          this.registerForm.controls['franchise-address'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "franchise_owner_cannot_confirmed", "isError" : true});
        } else {
          this.authService.authenticate(response._body,()=>{
            this.app.getRootNav().push(AuthComponent, {firstTime : true}).then(() => {
              this.app.getRootNav().remove(0, 1);
            });;
          });
          this.registerForm.resetForm();
        }
        this.loaderService.handleLoader(false);
      },
      error => {
        this.snackbarService.handleError({"message" : error.status + " " + error.statusText, "isError" : true, raw : true});
        this.loaderService.handleLoader(false);
      }
    );
  }

  ionViewWillEnter() {
     this.nav.swipeBackEnabled = false;
  }

  ionViewWillLeave() {
      this.nav.swipeBackEnabled = true;
  }
}
