import { Component, OnInit } from "@angular/core";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { RegisterComponent } from "../register/register.component";
import { NavController, App, AlertController } from "ionic-angular";
import { NgForm } from "@angular/forms";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { AuthComponent } from "../../Auth/auth.component";
import { AuthService } from "../../../app/auth/auth.service";
import { APIService } from "../../../app/auth/api.service";
import { ManagerComponent } from "../manager/manager.component";
import { TranslateService } from "../../../../node_modules/@ngx-translate/core";

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  manager = ManagerComponent;
  register = RegisterComponent;
  forgot = ForgotPasswordComponent;
  strings = {
    title: 'Enter Manager Mode',
    message: 'You will enter Manager Mode. Are you sure you want to continue?',
    cancel: 'Cancel',
    continue: 'Continue'
  };

  constructor(public navCtrl: NavController, private alertController: AlertController, private app : App, private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService, private api : APIService, private translateService: TranslateService) { }

  ngOnInit() {
    this.translateService.get('ent_man_code').subscribe((res) => this.strings.title = res);
    this.translateService.get('man_mode$question').subscribe((res) => this.strings.message = res);
    this.translateService.get('continue').subscribe((res) => this.strings.continue = res);
    this.translateService.get('cancel').subscribe((res) => this.strings.cancel = res);
  }

  onLogin(loginForm : NgForm) {
    if (loginForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.login(loginForm.value).subscribe(
      (response : any) => {
        if (response._body._m === "_emailinvalid") {
          loginForm.controls['email'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "email_unrecognized", "isError" : true});
        } else if (response._body._m === "_passinvalid") {
          loginForm.controls['password'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "incorrect_password", "isError" : true});
        } else if (response._body._m === "_accountlocked") {
          loginForm.controls['email'].setErrors({'invalid' : true});
          loginForm.controls['password'].setErrors({'invalid' : true});
          this.snackbarService.handleError({"message" : "account_locked", "isError" : true});
        } else {
          this.authService.authenticate(response._body, ()=>{
            this.app.getRootNav().push(AuthComponent).then(() => {
              this.loaderService.handleLoader(false);
              this.app.getRootNav().remove(0, 1);
            });
          });
          loginForm.resetForm();
          return;
        }
        this.loaderService.handleLoader(false);
      }
    );
  }

  onManagerMode() {
    let alert = this.alertController.create({
      title: this.strings.title,
      message: this.strings.message,
      buttons: [
        {
          text: this.strings.cancel,
          role: 'cancel',
          handler: () => {}
        },
        {
          text: this.strings.continue,
          handler: () => {
            localStorage.setItem('MANAGER_MODE', 'TRUE');
            localStorage.setItem('MANAGER_KEY', '');
            this.navCtrl.push(ManagerComponent);
          }
        }
      ]
    });
    alert.present();
  }
}
