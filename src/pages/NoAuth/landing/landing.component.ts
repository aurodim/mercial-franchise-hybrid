import { Component, ViewChild, OnInit, ElementRef } from "@angular/core";
import { Slides, NavController } from "ionic-angular";
import { RegisterComponent } from "../register/register.component";
import { TranslateService } from "../../../../node_modules/@ngx-translate/core";

@Component({
  selector: 'app-landing',
  templateUrl: 'landing.component.html'
})
export class LandingComponent implements OnInit {
  @ViewChild(Slides) slides: Slides;
  startFade : boolean = false;
  videoURL : string;
  videoPoster : string;

  constructor(public navCtrl: NavController) {
  }

  ngOnInit() {}

  ionViewDidLoad() {
    this.videoPoster = `https://res.cloudinary.com/aurodim/video/upload/w_${(window.innerWidth * 0.95).toFixed(0)}/v1545089913/mercial/static/explainers/business-explainer.png`;
    this.videoURL = `https://res.cloudinary.com/aurodim/video/upload/w_${(window.innerWidth * 0.95).toFixed(0)}/v1545089913/mercial/static/explainers/business-explainer.mp4`;
  }

  onSkipClick() {
    this.navCtrl.push(RegisterComponent);
  }

  onContinueClick() {
    this.navCtrl.push(RegisterComponent);
  }
}
