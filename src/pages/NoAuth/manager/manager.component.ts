import { Component } from "@angular/core";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { NavController, AlertController } from "ionic-angular";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { AuthService } from "../../../app/auth/auth.service";
import { Socket } from "ng-socket-io";
import { NoAuthComponent } from "../noauth.component";
import { TranslateService } from "../../../../node_modules/@ngx-translate/core";

@Component({
  selector: 'app-manager',
  templateUrl: 'manager.component.html'
})
export class ManagerComponent {
  success : boolean = false;
  socket: SocketIOClient.Socket;
  codePrompt : boolean = false;
  waiting : boolean = false;
  userCode : string = "";
  codeData : any = null;
  channel : string;
  error : boolean = false;
  savings: number = 0;
  timeout;
  strings = {
    title: 'Exit Manager Mode',
    message: 'You will exit Manager Mode. Are you sure you want to continue?',
    cancel: 'Cancel',
    continue: 'Continue'
  };
  mCodeSuccess = false;

  constructor(private loaderService : LoaderService, private translateService: TranslateService, private alertController: AlertController, private navController : NavController, private qrScanner: BarcodeScanner, private snackbarService : SnackbarService, private authService : AuthService, private socketIO : Socket) {
    this.socket = this.socketIO.connect();
    this.channel = this.makechannel() + "." + this.makechannel() + "." + this.makechannel();
  }

  ngOnInit() {
    this.translateService.get('ext_man_code').subscribe((res) => this.strings.title = res);
    this.translateService.get('man_modeext$question').subscribe((res) => this.strings.message = res);
    this.translateService.get('continue').subscribe((res) => this.strings.continue = res);
    this.translateService.get('cancel').subscribe((res) => this.strings.cancel = res);
    this.socket.on(this.channel, (data) => {
      clearTimeout(this.timeout);
      this.waiting = false;
      this.success = false;
      if (!data._s) {
        this.snackbarService.handleError({message : data._m, isError : true});
        setTimeout(()=>{
          this.retryScanner();
        }, 2000);
        return;
      }

      this.userCode = "";
      this.savings = data._r._s;
      this.loaderService.handleLoader(false);
      this.success = true;
    });
    if (localStorage.getItem("MANAGER_KEY")) {
      this.mCodeSuccess = true;
      this.codeData = {};
      this.codeData._m = {
        _c: localStorage.getItem("MANAGER_KEY"),
        _t: new Date().getTime()
      }
    }
  }

  ngOnDestroy() {
    this.socket.disconnect();
  }

  makechannel() : String {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^789()_";

    for (var i = 0; i < Math.floor(Math.random() * 120); i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  ngAfterViewInit() {
    this.continueScanning();
  }

  retryScanner() {
    this.error = false;
    this.continueScanning();
    if (this.codeData && this.codeData._m) {
      this.codeData = {
        _m: this.codeData._m
      };
    } else {
      this.codeData = {};
    }
    this.savings = 0;
    this.userCode = "";
    this.success = false;
    this.waiting = false;
    this.codePrompt = false;
    this.loaderService.handleLoader(false);
  }

  continueScanning() {
    //start scanning
    this.qrScanner.scan({preferFrontCamera:false,showTorchButton:true}).then((barcodeData) => {
      if (barcodeData.cancelled) {
        this.onBack();
        return;
      }

      try {
          let result = barcodeData.text;
          const data  = result.slice(0,-2).split(";");
          const idObj = data[0].split(":");
          const dateObj = data[2].split(":");

          if (!this.mCodeSuccess) {
            const managerObj = data[1].split(":");
            if (managerObj[0] !== "_m") {
              this.snackbarService.handleError({message : "invalid_man_qr", isError : true});
              this.retryScanner();
              return;
            }
            this.mCodeSuccess = true;
            localStorage.setItem("MANAGER_KEY", managerObj[1]);
            this.codeData = {};
            this.codeData._m = {
              _c: managerObj[1],
              _t: new Date().getTime()
            };
            this.continueScanning();
            return;
          }

          const franchisObj = data[1].split(":");
          let urcObj = null;

          if (data.length > 3) {
            urcObj = data[3].split(":");
          }

          if (idObj[0] !== "_i" || franchisObj[0] !== "_f" || dateObj[0] !== "_d") throw new Error("invalid_qr");

          this.codeData = {
            _o : idObj[1],
            _f : franchisObj[1],
            _od : dateObj[1],
            _d : null,
            _m : this.codeData._m,
            _a : (Math.random() * 100000).toFixed(0) + "__manager",
            _channel : this.channel
          };

          if (urcObj) {
            this.userCode = urcObj[1];
            this.waiting = true;
            this.loaderService.handleLoader(false);
            return this.onCodeValidate();
          }

          this.userCode = "";
          this.waiting = true;
          this.loaderService.handleLoader(false);
          this.codePrompt = true;
        } catch(error) {
          this.error = true;
          this.snackbarService.handleError({message : "invalid_qr", isError : true});
        }
    }, () => {
        this.snackbarService.handleError({ message: "error_scanning", isError: true });
      });
  }

  onAddCode(char: string) {
    this.userCode += char;
    let formattedkey = this.userCode.split('-').join('');
    if (formattedkey.length > 0) {
      formattedkey = formattedkey.match(new RegExp('.{1,4}', 'g')).join('-');
    }
    this.userCode = formattedkey;
    if (this.userCode.length == 9) this.onCodeValidate();
  }

  onDeleteCharacter() {
    if (this.userCode == "") {
      this.codeData = {
        _m : this.codeData._m,
      };
      this.retryScanner();
      return;
    }
    this.userCode = this.userCode.substring(0, this.userCode.length - 1);
    let formattedkey = this.userCode.split('-').join('');
    if (formattedkey.length > 0) {
      formattedkey = formattedkey.match(new RegExp('.{1,4}', 'g')).join('-');
    }
    this.userCode = formattedkey;
  }

  onCodeValidate() {
    if (!this.waiting && !this.codePrompt) {
      this.hideScanner();
      return;
    }

    this.codeData._d = new Date().getTime();
    this.codeData._c = this.userCode;

    this.loaderService.handleLoader(true);
    this.codePrompt = false;

    this.socket.emit('validateAttemptToRedeemByManager', this.codeData);

    this.timeout = setTimeout(() => {
      this.snackbarService.handleError({message : "prompt_timeout", isError : true});
      this.retryScanner();
    }, 20000);
  }

  showScanner() {
    this.userCode = "";
    this.loaderService.handleLoader(true);
    this.continueScanning();
  }

  hideScanner() {
    this.userCode = "";
    this.codeData = null;
    this.success = false;
    this.waiting = false;
    this.codePrompt = false;
    this.loaderService.handleLoader(false);
  }

  onBack() {
    let alert = this.alertController.create({
      title: this.strings.title,
      message: this.strings.message,
      buttons: [
        {
          text: this.strings.cancel,
          role: 'cancel',
          handler: () => {
            this.continueScanning();
          }
        },
        {
          text: this.strings.continue,
          handler: () => {
            this.navController.setRoot(NoAuthComponent);
            localStorage.removeItem('MANAGER_MODE');
            localStorage.removeItem('MANAGER_KEY');
          }
        }
      ]
    });
    alert.present();
  }
}
