import { Component } from "@angular/core";
import { NavControllerBase } from "ionic-angular";
import { LoginComponent } from "./login/login.component";
import { ManagerComponent } from "./manager/manager.component";
import { LandingComponent } from "./landing/landing.component";

@Component({
  selector: 'app-no-auth',
  templateUrl: 'noauth.component.html'
})
export class NoAuthComponent {
  navController: NavControllerBase;
  rootPage:any = LandingComponent;

  constructor() {
    if (localStorage.getItem("FIRST_TIME") != "FALSE") {
      localStorage.setItem("FIRST_TIME", "FALSE");
    } else if (localStorage.getItem("MANAGER_MODE") == 'TRUE') {
      this.rootPage = ManagerComponent;
    } else {
      this.rootPage = LoginComponent;
    }
  }
}
