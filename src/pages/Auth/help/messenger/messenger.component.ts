import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import {ApiAiClient} from "api-ai-javascript";

@Component({
  selector: 'app-help-messenger',
  templateUrl: 'messenger.component.html'
})
export class MessengerComponent {
  messages : object[] = [
    {
      type: "sender",
      message : "Hi! How can I help you? For command examples enter 'Help'"
    }
  ];

  onMessageSend(messageForm : NgForm, list) {

    if (messageForm.invalid) return;

    this.messages.push({
      type: "reciever",
      message : messageForm.value.message
    });

    new ApiAiClient({accessToken: '4cff5e48767c4f22a46e73e81bcab3e3'}).textRequest(messageForm.value.message).then(
      (response) => {
        this.messages.push({
          type: "sender",
          message : response.result.fulfillment.speech
        });
        setTimeout(()=>list.scrollTop = list.scrollHeight, 200);
      },
      () => {
        this.messages.push({
          type: "sender",
          message: "Error recieving message. Try Again."
        });
      }
    )

    messageForm.reset();
  }
}
