import { Component, ViewChild, Input, EventEmitter, Output, OnInit } from "@angular/core";
import { AuthService } from "../../../../app/auth/auth.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { APIService } from "../../../../app/auth/api.service";

@Component({
  selector : 'help-viewer',
  templateUrl: './viewer.helper.component.html'
})
export class HelpViewerComponent implements OnInit {
  @Input('video') video : string;
  @Output('hide') hideViewer : EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('viewerPlayer') viewerPlayer: any;
  showControls : boolean = true;
  constructor(private authService : AuthService, private snackbarService : SnackbarService, private loaderService : LoaderService, private api : APIService) {}

  ngOnInit() {}

  playVideo(event: any) {
    this.showControls = false;
    this.viewerPlayer.nativeElement.play();
  }

  onHide() {
    if (this.viewerPlayer) {
      this.viewerPlayer.nativeElement.pause();
      this.viewerPlayer.nativeElement.src = "";
    }
    this.hideViewer.emit(true);
  }

  getThumbnail() {
    let vCopy = this.video.split(".").slice();
    vCopy.pop();
    return vCopy.join(".") + ".png";
  }
}
