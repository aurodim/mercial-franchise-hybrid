import { Component, ViewChild } from "@angular/core";
import { GoogleMap } from "@agm/core/services/google-maps-types";
import { MapsAPILoader } from "@agm/core";
import { APIService } from "../../../app/auth/api.service";
import { AuthService } from "../../../app/auth/auth.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html"
})
export class DashboardComponent {
  offersPosted : number;
  moneyMade : number;
  currentTab : number;
  showTab : boolean = false;
  offersRedeemed : number;
  rank : number;
  rating_raw : number;
  rating : number[];
  rhalf : boolean;
  nearMe : number;
  locations : any[];
  zoom : number = 19;

  @ViewChild("map") map : GoogleMap;
  bounds : google.maps.LatLngBounds;

  public customersData:Array<any> = [
    {data: [0], label : "New"},
    {data: [0], label : "Returning"}
  ];
  public nearbyData:Array<any> = [
    {data: [0], label : "Nearby"}
  ];
  public lineChartLabels:Array<any> = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  public lineChartOptions:any = {
    responsive: true,
    scales: {
            yAxes: [{
                ticks: {
                    beginAtZero : true
                }
            }]
        }
  };
  public customersChartColors:Array<any> = [];
  public nearbyChartColors:Array<any> = [];
  public lineChartType:string = 'line';


  constructor(private mapsAPILoader: MapsAPILoader, private api : APIService, private authService : AuthService, private loaderService : LoaderService, private translate : TranslateService) {  }

  ionViewWillEnter() {
    this.translate.get("months").subscribe(res => this.lineChartLabels = res);
    this.translate.get("new").subscribe(res => this.customersData[0].label = res);
    this.translate.get("returning").subscribe(res => this.customersData[1].label = res);
    this.translate.get("nearby").subscribe(res => this.nearbyData[0].label = res);
    this.api.analyticsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.offersPosted = res._op;
        this.moneyMade = res._mm;
        this.currentTab = res._ct;
        this.authService.setTab(this.currentTab);
        this.showTab = this.currentTab > 5;
        this.offersRedeemed = res._or;
        this.rank = res._r;
        this.nearMe = res._nm;
        this.locations = res._l;
        this.rating_raw = res._ra;
        this.rating = Array(Math.trunc(res._ra)).fill(1);
        this.rhalf = res._ra - Math.trunc(res._ra) > 0.34 && res._ra - Math.trunc(res._ra) < 0.75;
        this.locations = res._l;
        this.customersData[0].data = res._cd.n;
        this.customersData[1].data = res._cd.r;
        this.customersChartColors = res._cd.c;
        this.nearbyData[0].data = res._nd.d;
        this.nearbyChartColors = res._nd.c;
        this.mapsAPILoader.load().then(() => this.mapAuto());
      }
    );
  }

  onRefresh($event) {
    this.loaderService.handleLoader(true);
    this.api.analyticsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.offersPosted = res._op;
        this.moneyMade = res._mm;
        this.currentTab = res._ct;
        this.offersRedeemed = res._or;
        this.rank = res._r;
        this.authService.setTab(this.currentTab);
        this.nearMe = res._nm;
        this.locations = res._l;
        this.rating_raw = res._ra;
        this.rating = Array(Math.trunc(res._ra)).fill(1);
        this.rhalf = res._ra - Math.trunc(res._ra) > 0.34 && res._ra - Math.trunc(res._ra) < 0.75;
        this.locations = res._l;
        this.customersData[0].data = res._cd.n;
        this.customersData[1].data = res._cd.r;
        this.customersChartColors = res._cd.c;
        this.nearbyData[0].data = res._nd.d;
        this.nearbyChartColors = res._nd.c;

        this.mapsAPILoader.load().then(() => this.mapAuto());
        $event.complete();
      }
    );
  }

  mapAuto() {
    if (this.locations.length > 1) {
      this.bounds = new google.maps.LatLngBounds();
      for (let location of this.locations) {
        const loc = new google.maps.LatLng(location.lat, location.lng);
        this.bounds.extend(loc);
      }
    }
    this.loaderService.handleLoader(false);
  }
}
