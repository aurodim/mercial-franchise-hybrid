import { Component, OnInit, OnDestroy } from "@angular/core";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { AuthService } from "../../../app/auth/auth.service";
import { Socket } from "ng-socket-io";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NavController } from "ionic-angular";

@Component({
  selector: 'app-scanner',
  templateUrl: 'scanner.component.html'
})
export class ScannerComponent implements OnInit, OnDestroy {
  success : boolean = false;
  socket: SocketIOClient.Socket;
  codePrompt : boolean = false;
  waiting : boolean = false;
  userCode : string = "";
  codeData : any = null;
  channel : string;
  timeout;

  constructor(private loaderService : LoaderService, private navController : NavController, private qrScanner: BarcodeScanner, private snackbarService : SnackbarService, private authService : AuthService, private socketIO : Socket) {
    this.socket = this.socketIO.connect();
    this.channel = this.makechannel() + "." + this.makechannel() + "." + this.makechannel();
  }

  ngOnInit() {
    this.socket.on(this.channel, (data) => {
      clearTimeout(this.timeout);
      this.waiting = false;
      this.success = false;
      if (!data._s) {
        this.snackbarService.handleError({message : data._m, isError : true});
        setTimeout(() => {
          this.retryScanner();
        }, 2500)
        return;
      }
      this.hideScanner();
      this.authService.addTab(data._r._r);
      this.userCode = "";
      this.loaderService.handleLoader(false);
      this.success = true;
    });
  }

  ngOnDestroy() {
    this.socket.disconnect();
  }

  makechannel() : String {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^789()_";

    for (var i = 0; i < Math.floor(Math.random() * 120); i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  ngAfterViewInit() {
    this.continueScanning();
  }

  retryScanner() {
    this.continueScanning();
    this.codeData = null;
    this.userCode = "";
    this.success = false;
    this.waiting = false;
    this.codePrompt = false;
    this.loaderService.handleLoader(false);
  }

  continueScanning() {
    //start scanning
    this.qrScanner.scan({preferFrontCamera:false,showTorchButton:true, resultDisplayDuration: 0}).then((barcodeData) => {
      if (barcodeData.cancelled) {
        this.navController.pop();
        return;
      }

      try {
          let result = barcodeData.text;
          const data  = result.slice(0,-2).split(";");
          const idObj = data[0].split(":");
          const franchisObj = data[1].split(":");
          const dateObj = data[2].split(":");
          let urcObj = null;

          if (data.length > 3) {
            urcObj = data[3].split(":");
          }

          this.codeData = {
            _o : idObj[1],
            _f : franchisObj[1],
            _od : dateObj[1],
            _d : null,
            _a : this.authService.getAUTH(),
            _channel : this.channel
          };

          if (urcObj) {
            this.userCode = urcObj[1];
            this.waiting = true;
            this.loaderService.handleLoader(false);
            return this.onCodeValidate();
          }

          this.userCode = "";
          this.waiting = true;
          this.loaderService.handleLoader(false);
          this.codePrompt = true;
        } catch(error) {
          this.retryScanner();
          this.snackbarService.handleError({message : "invalid_qr", isError : true});
        }
    }, () => {
        this.snackbarService.handleError({ message: "error_scanning", isError: true });
      });
  }

  onAddCode(char: string) {
    this.userCode += char;
    let formattedkey = this.userCode.split('-').join('');
    if (formattedkey.length > 0) {
      formattedkey = formattedkey.match(new RegExp('.{1,4}', 'g')).join('-');
    }
    this.userCode = formattedkey;
    if (this.userCode.length == 9) this.onCodeValidate();
  }

  onDeleteCharacter() {
    if (this.userCode == "") {
      this.retryScanner();
      return;
    }
    this.userCode = this.userCode.substring(0, this.userCode.length - 1);
    let formattedkey = this.userCode.split('-').join('');
    if (formattedkey.length > 0) {
      formattedkey = formattedkey.match(new RegExp('.{1,4}', 'g')).join('-');
    }
    this.userCode = formattedkey;
  }

  onCodeValidate() {
    if (!this.waiting && !this.codePrompt) {
      this.hideScanner();
      return;
    }

    this.codeData._d = new Date().getTime();
    this.codeData._c = this.userCode;

    this.loaderService.handleLoader(true);
    this.codePrompt = false;

    this.socket.emit('validateAttemptToRedeem', this.codeData);

    this.timeout = setTimeout(() => {
      this.snackbarService.handleError({message : "prompt_timeout", isError : true});
      this.retryScanner();
    }, 20000);
  }

  showScanner() {
    this.userCode = "";
    this.loaderService.handleLoader(true);
    this.continueScanning();
  }

  hideScanner() {
    this.userCode = "";
    this.codeData = null;
    this.success = false;
    this.waiting = false;
    this.codePrompt = false;
    this.loaderService.handleLoader(false);
  }
}
