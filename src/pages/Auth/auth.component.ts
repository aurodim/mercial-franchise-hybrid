import { Component, ViewChild, OnInit } from "@angular/core";
import { NavController, App, Platform, Tabs, ModalController, AlertController, NavParams } from "ionic-angular";
import { AuthService } from "../../app/auth/auth.service";
import { HelpComponent } from "./help/help.component";
import { SettingsComponent } from "./settings/settings.component";
import { NoAuthComponent } from "../NoAuth/noauth.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { OffersComponent } from "./offers/offers.component";
import { ScannerComponent } from "./scanner/scanner.component";
import { MessengerComponent } from "./help/messenger/messenger.component";
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { RedeemsComponent } from "./redeems/redeems.component";
import { APIService } from "../../app/auth/api.service";
import { SnackbarService } from "../../app/snackbar/snackbar.service";
import { TranslateService } from "../../../node_modules/@ngx-translate/core";
import { Subscription } from "../../../node_modules/rxjs";
import { HelperViewerService } from "./help/viewer/viewer.helper.service";
import { ThreeDeeTouch } from "@ionic-native/three-dee-touch";

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html'
})
export class AuthComponent implements OnInit {
  @ViewChild('tabsRef') tabRef : Tabs;
  navController : NavController;
  Dashboard = DashboardComponent;
  Offers = OffersComponent;
  Redeems = RedeemsComponent;
  Help = HelpComponent;
  Settings = SettingsComponent;
  tabsOrder = [0,1,2];
  tabIndex: number = 0;
  loaded: boolean = false;
  showHelper = false;
  helperURL : string = "";
  subscription: Subscription;
  logOutEmition: Subscription;
  translationStrings = [];

  constructor(private app : App, private api : APIService, private threeDeeTouch : ThreeDeeTouch, private snackbarService : SnackbarService, private modalCtrl : ModalController, private nativePageTransitions : NativePageTransitions, private platform : Platform, private authService : AuthService, public alertCtrl: AlertController, private translate : TranslateService, private helperViewerService : HelperViewerService, private navParams: NavParams) {
    this.navController = this.app.getRootNav();
   this.logOutEmition = this.authService.getSubcription().subscribe((logout) => {
     if (!logout) return;
     this.navController.push(NoAuthComponent).then(()=>{
       this.navController.remove(0,1);
     });
   });
   this.forceTouch();
   this.subscription = this.helperViewerService.getTrigger().subscribe((data) => { this.showHelper = data.show; this.helperURL = data.url });
  }

  ngOnInit() {
    this.translate.get("redeem_tab$question").subscribe(res => this.translationStrings.push(res));
    this.translate.get("redeem_tab_message").subscribe(res => this.translationStrings.push(res));
    this.translate.get("not_yet").subscribe(res => this.translationStrings.push(res));
    this.translate.get("continue").subscribe(res => this.translationStrings.push(res));
  }

  ionViewDidLoad() {
    this.navController = this.app.getRootNav();
    var firstTime : boolean = this.navParams.get("firstTime");
    if (firstTime) {
      this.helperURL = "https://res.cloudinary.com/aurodim/video/upload/v1546473150/mercial/static/explainers/business-advertiser-beginner.mp4";
      this.showHelper = true;
    }
  }

  onSwipe($event) {
    if ($event.overallVelocityX > 0) {
      let ind = this.tabRef.getSelected().index - 1;
      if (ind == -1) return;
      this.tabRef.select(ind);
    } else {
      let ind = this.tabRef.getSelected().index + 1;
      if (ind == 5) return;
      this.tabRef.select(ind);
    }
  }

  onTabChange($event) {
    if ($event.index == 0 || $event.index == 5) return;

    this.tabsOrder = [$event.index - 1, $event.index, $event.index + 1];

    if (!this.loaded) {
      this.loaded = true;
      return;
    }
  }

  forceTouch() {
    try {
      this.threeDeeTouch.configureQuickActions([
        {
          title : "Scan Code",
          iconTemplate : "CapturePhoto",
          iconType: "CapturePhoto"
        }
      ]);

      this.threeDeeTouch.onHomeIconPressed().subscribe(
       (payload) => {
         // returns an object that is the button you pressed

         if (payload.title === "Scan Code") {
           this.modalCtrl.create(ScannerComponent).present();
         }
       }
      )
    } catch(Error) {

    }
  }

  redeemTab() {
    const confirm = this.alertCtrl.create({
      title: this.translationStrings[0],
      message: this.translationStrings[1],
      buttons: [
        {
          text: this.translationStrings[2],
          handler: () => {

          }
        },
        {
          text: this.translationStrings[3],
          handler: () => {
            this.api.redeemTab({auth : this.authService.getAUTH()}).subscribe((res : any) => {
              res = res._body;
              if (res._m == "_tabtoolow") {
                this.snackbarService.handleError({"message" : "tab_low_error", "isError" : true});
              } else if (res._m == "_invalidcc") {
                this.snackbarService.handleError({"message" : "invalid_card_details", "isError" : true});
              } else if (res._m == "_errorpayout") {
                this.snackbarService.handleError({"message" : "tab_payout_error", "isError" : true});
              } else {
                this.snackbarService.handleError({"message" : "tab_redeemed", "isError" : false});
              }

              this.authService.setTab(0);
            });
          }
        }
      ]
    });
    confirm.present();
  }

  onLogout() {
    this.authService.logout();
  }

  onMessageOpen() {
    let messageModal = this.modalCtrl.create(MessengerComponent);
    messageModal.present();
  }

  onRedeem() {
    this.modalCtrl.create(ScannerComponent).present();
  }

  ngOnDestroy() {
      this.logOutEmition.unsubscribe();
      this.subscription.unsubscribe();
   }
}
