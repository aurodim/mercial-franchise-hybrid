import { Component } from "@angular/core";
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { ModalController } from "ionic-angular";
import { EditOfferComponent } from "../offers/edit/edit-offer.component";

@Component({
  selector: 'app-redeems',
  templateUrl: 'redeems.component.html'
})
export class RedeemsComponent {
  redeems : any[] = [];

  constructor(private api : APIService, public modalCtrl: ModalController, private loaderService : LoaderService) {}

  ionViewWillEnter() {
    this.api.redeemsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.redeems = res._r;
        this.loaderService.handleLoader(false);
      }
    );
  }

  onRefresh($event) {
    this.loaderService.handleLoader(true);
    this.api.redeemsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.redeems = res._r;
        this.loaderService.handleLoader(false);
        $event.complete();
      }
    );
  }

  onView(index : number) {
    let editModal = this.modalCtrl.create(EditOfferComponent, { id: this.redeems[index].id });
    editModal.present();
  }
}
