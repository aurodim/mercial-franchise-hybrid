import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { FranchiseOffer } from "./offer/offer.model";
import { APIService } from "../../../app/auth/api.service";
import { LoaderService } from "../../../app/loader/loader.service";

@Injectable()
export class FranchiseOfferService {
  offersChanged = new Subject<FranchiseOffer[]>();

  private offers : FranchiseOffer[] = [];

  constructor(private api : APIService, private loaderService : LoaderService) {}

  requestOffers(callback : Function) {
    this.offers = [];
    this.api.offersData().subscribe(
      (response : any) => {
        const res = response._body._d;
        for (let offer of res._o) {
          this.offers.push(new FranchiseOffer(offer.id, offer.name, offer.image, offer.locations, offer.website, offer.merchips, offer.savings, offer.description))
        }
        this.loaderService.handleLoader(false);
        callback();
      }
    );
  }

  reloadOffers() {
    this.offers = [];
    this.api.offersData().subscribe(
      (response : any) => {
        const res = response._body._d;
        for (let offer of res._o) {
          this.offers.push(new FranchiseOffer(offer.id, offer.name, offer.image, offer.locations, offer.website, offer.merchips, offer.savings, offer.description))
        }
        this.offersChanged.next(this.offers.slice());
        this.loaderService.handleLoader(false);
      }
    );
  }

  getOffers() {
    return this.offers.slice();
  }

  getOffer(id:string) {
    return this.offers.find(offer => offer.id === id);
  }

  add(offer : FranchiseOffer) {
    this.offers.unshift(new FranchiseOffer(offer.id, offer.name, offer.image, offer.locations, offer.website, offer.merchips, offer.savings, offer.description));
    this.offersChanged.next(this.offers.slice());
  }

  remove(id : string) {
    this.offers = this.offers.filter((offer) => offer.id != id);
    this.offersChanged.next(this.offers.slice());
  }
}
