import { Component, OnInit } from "@angular/core";
import { APIService } from "../../../../app/auth/api.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { NavParams, NavController } from "ionic-angular";
import { TranslateService } from "@ngx-translate/core";
import { BaseChartDirective } from "ng2-charts";
import { FranchiseOfferService } from "../offers.service";

@Component({
  selector: 'app-analytics-offer',
  templateUrl: './analytics-offer.component.html'
})
export class OfferAnalyticsComponent implements OnInit {
  id : string;
  public lineChartData:Array<any> = [
    {data: [], label: 'Views'}
  ];
  zoomed : boolean = false;
  public lineChartType:string = 'line';
  public lineChartLabels:Array<any> = [];
  public lineChartOptions:any = {
    responsive: true,
    scales: {
        yAxes: [{
          display: false,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                beginAtZero: true,   // minimum value will be 0.
                fontColor: "rgba(102,51,153,0.8)",
                fontSize: 14,
                padding: 20,
                max: 20
            },
            gridLines: {
                display:false,
                color: "rgba(0,0,0,0.1)"
            }
        }],
        xAxes: [{
            ticks: {
              fontColor: "rgba(102,51,153,0.8)",
                fontSize: 14,
                beginAtZero: true
            },
            gridLines: {
                display:false,
                color: "rgba(0,0,0,0.1)"
            }
        }]
    },
    legend: {
        labels: {
            fontColor: "rgba(102,51,153,0.8)",
            fontSize: 14
        }
    },
  };
  public lineColors:Array<any> = [{
    backgroundColor: 'rgba(102,51,153,0.3)',
    borderColor: '#663399',
    pointBackgroundColor: '#663399',
    pointBorderColor: '#663399',
    pointHoverBackgroundColor: '#663399',
    pointHoverBorderColor: '#663399'
  }];
  redeems : number = 0;
  lifespan : number = 0;
  ready = false;
  name : string;
  refund : number;
  helpedSaved : number;
  public data = [];

  constructor(private api : APIService, private navController : NavController, private params : NavParams, private offersService : FranchiseOfferService, private loaderService : LoaderService, private snackbarService : SnackbarService, private translate: TranslateService) {}

  ngOnInit() {
    this.translate.get("views").subscribe(res => this.lineChartData[0].label = res);
    this.id = this.params.get("id");
    this.api.offersAnalyticsData(this.id).subscribe(
      (response : any) => {
        const res = response._body._d;

        if (!res._v) {
          this.snackbarService.handleError({"message" : "offer_not_found", "isError" : true})
          this.loaderService.handleLoader(false);
          this.navController.pop();
          return;
        }

        this.name = res._n;
        this.redeems = res._r;
        this.refund = res._mb;
        this.helpedSaved = res._hs;
        this.lifespan = res._l;
        this.data = res._c;

        this.lineChartData = [{data: this.data[0].data, label: ""}];
        this.lineChartLabels = this.data[0].labels;

        if (this.data[0].data.length == 1) {
          this.zoomed = true;
          this.lineChartData = [{data: this.data[1].data, label: ""}];
          this.lineChartLabels = this.data[1].labels;
          this.lineChartOptions.scales.yAxes[0].ticks.max = Math.max(...this.data[1].data)+2;
        } else {
          this.lineChartData = [{data: this.data[0].data, label: ''}];
          this.lineChartLabels = this.data[0].labels;
          this.lineChartOptions.scales.yAxes[0].ticks.max = Math.max(...this.data[0].data)+2;
        }

        this.ready = true;
        this.loaderService.handleLoader(false);
      }
    );
  }

  chartClicked(e:any):void {
    if (e.active.length > 0 && !this.zoomed) {
      if (!this.data[e.active[0]._index + 1]) return;
      this.lineChartOptions.scales.yAxes[0].ticks.max = Math.max(...this.data[e.active[0]._index + 1].data)+2;
      this.lineChartData[0] = Object.assign({}, this.lineChartData[0], {
        data: [...this.data[e.active[0]._index + 1].data], label: this.data[e.active[0]._index + 1].labels[0]
      });
      this.lineChartLabels = [...this.data[e.active[0]._index + 1].labels];
      this.zoomed = true;
    } else {
      if (this.data.length == 0) return;
      if (this.data[0].data.length == 1) return;
      this.zoomed = false;
      this.lineChartOptions.scales.yAxes[0].ticks.max = Math.max(...this.data[0].data)+2;
      this.lineChartData[0] = Object.assign({}, this.lineChartData[0], {
        data: [...this.data[0].data], label: ''
      });
      this.lineChartLabels = [...this.data[0].labels];
    }
  }
}
