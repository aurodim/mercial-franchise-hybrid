import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, NgForm } from "@angular/forms";
import { AuthService } from "../../../../app/auth/auth.service";
import { APIService } from "../../../../app/auth/api.service";
import { FranchiseOfferService } from "../offers.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { NavParams, NavController } from "ionic-angular";

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.component.html'
})
export class EditOfferComponent implements OnInit {
  locations = {
    "all" : false,
    "selected" : 0,
    "address" : []
  };
  pageNumber = 1;
  id : string;

  editForm = new FormGroup({
    auth : new FormControl(this.api.AUTH(), [Validators.required]),
    id : new FormControl(this.id, [Validators.required]),
    title : new FormControl('', [Validators.required]),
    description : new FormControl(''),
    "locations" : new FormControl('', [Validators.required]),
    savings : new FormControl([], [Validators.required, Validators.pattern(/^[$]?[\.]?[0-9]+([.][0-9]{2})?$/g)]),
    refund : new FormControl([], [Validators.required, Validators.pattern(/^[$]?[\.]?[0-9]+([.][0-9]{2})?$/g)])
  });

  constructor(private authService : AuthService, private navParams : NavParams, private navController : NavController, private api : APIService, private offersService : FranchiseOfferService, private loaderService : LoaderService, private snackbarService : SnackbarService) {}

  ngOnInit() {
    this.id = this.navParams.get("id");
    this.api.editorData(this.id, () => this.navController.pop()).subscribe(
      (response : any) => {
        const res = response._body._d;

        if (!res._o) {
          this.snackbarService.handleError({"message" : "offer_not_found", "isError" : true})
          this.loaderService.handleLoader(false);
          this.navController.pop();
        }

        this.locations.address = res._l;
        this.editForm.controls['locations'].setValue(JSON.stringify(this.locations.address));
        this.editForm.controls["id"].setValue(res._o.id);
        this.editForm.controls["title"].setValue(res._o.name);
        this.editForm.controls["description"].setValue(res._o.description);
        this.editForm.controls["savings"].setValue(res._o.savings);
        this.loaderService.handleLoader(false);
      }
    );
  }

  locationSelected($event, index) {
    this.locations.address[index].c = $event.checked;

    let checked = 0;

    for (let location of this.locations.address) {
      if (!location.c) {
        this.locations.all = false;
      } else {
        checked += 1;
      }
    }

    this.locations.selected = checked;
    this.editForm.controls['locations'].setValue(JSON.stringify(this.locations.address));
  }

  allLocationsChecked($event) {
    for (let location of this.locations.address) {
      location.c = $event.checked;
    }
    this.locations.selected = this.locations.address.length;
    this.editForm.controls['locations'].setValue(JSON.stringify(this.locations.address));
  }

  onDelete() {
    let obj = {
      auth : this.authService.getAUTH(),
      id : this.id,
      d : new Date().getTime()
    };
    this.api.deleteOffer(obj).subscribe(
      () => {
        this.snackbarService.handleError({ "message": "offer_deleted", "isError": false });
        this.offersService.remove(this.id);
        this.loaderService.handleLoader(false);
        this.navController.pop();
      }
    );
  }

  onSubmit() {
    if (this.editForm.invalid) {
      return;
    }

    this.loaderService.handleLoader(true);
    this.api.updateOffer(this.editForm.value).subscribe(
      () => {
        this.snackbarService.handleError({ "message": "offer_updated", "isError": false });
        this.editForm.reset(this.editForm.value);
        this.loaderService.handleLoader(false);
        this.offersService.reloadOffers();
        this.navController.pop();
      }
    );
  }

  onBack() {
    this.pageNumber -= 1;
  }

  onNext() {
    this.pageNumber += 1;
  }
}
