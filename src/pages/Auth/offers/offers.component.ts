import { Component, ViewChild } from "@angular/core";
import { FabContainer, ModalController } from "ionic-angular";
import { Subscription } from "rxjs";
import { FranchiseOfferService } from "./offers.service";
import { LoaderService } from "../../../app/loader/loader.service";
import { FranchiseOffer } from "./offer/offer.model";
import { NewOfferComponent } from "../offers/new/new-offer.component";

@Component({
  selector: 'app-offers',
  templateUrl: 'offers.component.html'
})
export class OffersComponent {
  @ViewChild("fab") fab : FabContainer;
  subscription : Subscription;
  offers : FranchiseOffer[] = [];

  constructor(private offerService : FranchiseOfferService, private loaderService : LoaderService, private modalCtrl : ModalController) {}

  ionViewWillEnter() {
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
    });
    this.subscription = this.offerService.offersChanged.subscribe(
      (offers : FranchiseOffer[]) => {
        this.offers = offers;
      }
    );
  }

  onRefresh($event) {
    this.loaderService.handleLoader(true);
    this.offerService.requestOffers(() => {
      this.offers = this.offerService.getOffers();
      $event.complete();
    });
  }

  onAddOffer() {
    this.fab.close();
    this.modalCtrl.create(NewOfferComponent).present();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
