import { Component, Input, OnInit } from "@angular/core";
import { FranchiseOffer } from "./offer.model";
import { ModalController, AlertController } from "ionic-angular";
import { EditOfferComponent } from "../edit/edit-offer.component";
import { OfferAnalyticsComponent } from "../analytics/analytics-offer.component";
import { TranslateService } from "../../../../../node_modules/@ngx-translate/core";

@Component({
  selector: 'app-franchise-offer',
  templateUrl: './offer.component.html'
})
export class OfferComponent implements OnInit {
  @Input() offer : FranchiseOffer;
  translationStrings = [];

  constructor(public modalCtrl: ModalController, private alertCtrl : AlertController, private translate : TranslateService) {

  }

  ngOnInit() {
    this.translate.get("locations").subscribe(res => this.translationStrings.push(res));
    this.translate.get("ok").subscribe(res => this.translationStrings.push(res));
  }

  onAddressView() {
    let alert = this.alertCtrl.create({
      title: this.translationStrings[0],
      message: this.offer.locations.slice().map(location => location.formatted).join("<br/><br/>"),
      buttons: [this.translationStrings[1]]
    });
    alert.present();
  }

  onView() {
    let editModal = this.modalCtrl.create(EditOfferComponent, { id: this.offer.id });
    editModal.present();
  }

  onAnalytics() {
    let analyticsModal = this.modalCtrl.create(OfferAnalyticsComponent, { id: this.offer.id });
    analyticsModal.present();
  }

}
