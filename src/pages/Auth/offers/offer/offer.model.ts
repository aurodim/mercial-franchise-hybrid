export class FranchiseOffer {
  id : string;
  name : string;
  description? : string;
  image : string;
  locations : any[];
  website : string;
  merchips : number;
  savings : string;

  constructor(id : string, name : string, image : string, locations : any[], website : string, merchips : number, savings : string, description? : string) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.image = image;
    this.locations = locations;
    this.website = website;
    this.merchips = merchips;
    this.savings = savings;
  }
}
