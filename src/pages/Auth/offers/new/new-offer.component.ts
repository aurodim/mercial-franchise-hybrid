import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { FranchiseOfferService } from "../offers.service";
import { APIService } from "../../../../app/auth/api.service";
import { LoaderService } from "../../../../app/loader/loader.service";
import { SnackbarService } from "../../../../app/snackbar/snackbar.service";
import { NavController } from "ionic-angular";
import { AuthService } from "../../../../app/auth/auth.service";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

@Component({
  selector: 'app-franchise-new-offer',
  templateUrl: './new-offer.component.html'
})
export class NewOfferComponent implements OnInit {
  urlValidator : string | RegExp = new RegExp(/(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/i);
  moneyValidator : string | RegExp = new RegExp(/^[$]?[\.]?[0-9]+([.][0-9]{2})?$/);
  locations = {
    "all" : false,
    "selected" : 0,
    "address" : []
  };
  pageNumber = 1;
  fileURI:any;
  fileName:string;

  constructor(private authService : AuthService, private navController : NavController, private api : APIService, private offerService : FranchiseOfferService, private loaderService : LoaderService, private snackbarService : SnackbarService, private transfer: FileTransfer, private camera: Camera) {}

  ngOnInit() {
    this.api.locationsData(() => this.navController.pop()).subscribe(
      (response : any) => {
        const res = response._body._d;
        this.locations.address = res._l;
        this.loaderService.handleLoader(false);
      }
    );
  }

  locationSelected($event, index, newOfferForm : NgForm) {
    this.locations.address[index].c = $event.checked;

    let checked = 0;

    for (let location of this.locations.address) {
      if (!location.c) {
        this.locations.all = false;
      } else {
        checked += 1;
      }
    }

    this.locations.selected = checked;
  }

  allLocationsChecked($event) {
    this.locations.all = $event.checked;
    for (let location of this.locations.address) {
      location.c = $event.checked;
      this.locations.selected = this.locations.address.length;
    }
  }

  onSubmit(newOfferForm : NgForm) {
    if (newOfferForm.invalid || !this.fileURI) {
      return;
    }

    this.loaderService.handleLoader(true);
    const fileTransfer: FileTransferObject = this.transfer.create();
    let fName = this.fileName;
    let options: FileUploadOptions = {
      fileKey: 'thumbnail',
      fileName: 'offer-thumbnail-'+new Date().getTime()+'.'+fName.split('.')[1],
      chunkedMode: false,
      mimeType: "image/jpeg/"+fName.split('.')[1],
      headers: {"Accept" : 'application/json'},
      params: {
        'auth' : this.authService.getAUTH(),
        'description' : newOfferForm.value.description,
        'title' : newOfferForm.value.title,
        'savings' : newOfferForm.value.savings,
        'refund' : newOfferForm.value.refund,
        'locations' : JSON.stringify(this.locations.address)
      }
    };

    fileTransfer.upload(this.fileURI, this.api.api + '/api/franchise/offers/new', options)
      .then((response : any) => {

        const res = JSON.parse(response.response)._body;

        this.offerService.add(res._o);
        this.snackbarService.handleError({"message" : "offer_uploaded", "isError" : false})
        newOfferForm.reset(newOfferForm.value);
        this.loaderService.handleLoader(false);
        this.navController.pop();
    }, () => {
          this.loaderService.handleLoader(false);
          this.snackbarService.handleError({ message: 'offer_upload_error', isError: true });
        });
  }

  onChooseThumbnail() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      this.fileURI = imageData;
      this.fileName = imageData.toString();
    }, (err) => {
      if (err === "has no access to assets") {
        return;
      }
      this.snackbarService.handleError({message : "image_select_error", isError : true})
    });
  }

  onBack() {
    this.pageNumber -= 1;
  }

  onNext() {
    this.pageNumber += 1;
  }
}
