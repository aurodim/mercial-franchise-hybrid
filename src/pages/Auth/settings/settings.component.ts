import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LoaderService } from "../../../app/loader/loader.service";
import { SnackbarService } from "../../../app/snackbar/snackbar.service";
import { APIService } from "../../../app/auth/api.service";
import { AuthService } from "../../../app/auth/auth.service";
import { NoAuthComponent } from "../../NoAuth/noauth.component";
import { App, ModalController, Select } from "ionic-angular";
import { MapsAPILoader } from "@agm/core";
import { AutoCompleteLocationComponent } from "../../shared/autocomplete-location.component";
import { Stripe, StripeCardTokenParams } from "@ionic-native/stripe";
import { LegalViewerComponent } from "../../Legal/legal.viewer.component";
import { TranslateService } from "../../../../node_modules/@ngx-translate/core";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { StatusBar } from "@ionic-native/status-bar";

@Component({
  selector: "app-settings",
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  @ViewChild('legalSelect') legalSelect: Select;
  franchise_place = null;
  legal_place = null;
  legal_valid : boolean = false;
  stripeLogin = "";
  fcode : string;
  locations : any[];
  today : string = new Date().toISOString();
  managerCode = null;
  managerCodeShow = false;
  stripe_source_tkn = "";
  businessTypes = [
    {value : 'individual', formatted: "Individual"},
    {value : 'company', formatted: "Company"}
  ];
  legalDocs = [
    {value : 'busiGuide', formatted: "Guidelines"},
    {value : 'privacy-policy', formatted: "Privacy Policy"},
    {value : 'terms-of-use', formatted: "Terms of Service"},
    {value: 'dmca', formatted: "DMCA"},
    {value : 'disclaimer', formatted: "Disclaimer"}
  ];
  fcodeImage : string;
  passwordDisplay : string = "password";
  deletionIsValid : boolean = false;
  accountEmail : string;
  accountForm = new FormGroup({
    auth : new FormControl(this.authService.getAUTH()),
    email : new FormControl('', { validators: [Validators.email, Validators.required], updateOn: 'blur' }),
    password : new FormControl('')
  });
  franchiseForm = new FormGroup({
    auth : new FormControl(this.authService.getAUTH()),
    name : new FormControl('', { validators: [Validators.required] }),
    owner : new FormControl('', { validators: [Validators.required] }),
    website : new FormControl('', { validators: [Validators.pattern(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/giy)]})
  });
  locationForm = new FormGroup({
    auth : new FormControl(this.authService.getAUTH()),
    address : new FormControl('', { validators: [Validators.required] }),
    lat : new FormControl('', { validators: [Validators.required] }),
    lng : new FormControl('', { validators: [Validators.required] }),
    postal : new FormControl('', { validators: [Validators.required] }),
    previous: new FormControl(null)
  });
  managerForm = new FormGroup({
    auth : new FormControl(this.authService.getAUTH())
  });
  privacyForm = new FormGroup({
    auth : new FormControl(this.authService.getAUTH()),
    notif : new FormControl(false)
  });
  deleteAccountForm = new FormGroup({
    auth : new FormControl(this.authService.getAUTH()),
    email : new FormControl({value : '', disabled : false}, [Validators.email, Validators.required]),
  });

  constructor(private api : APIService, private statusBar: StatusBar, private translate: TranslateService, private app: App, private modalController : ModalController, private authService : AuthService, private loaderService : LoaderService, private iab: InAppBrowser, private snackbarService : SnackbarService, private stripe: Stripe) {
    this.stripe.setPublishableKey(process.env.STRIPE_PUBLIC);
  }

  ngOnInit() {
    this.translate.get("legal$list").subscribe((res : string[]) => {
      for (let i = 0; i < res.length; i++) {
        this.legalDocs[i].formatted = res[i];
      }
    });
    // this.translate.get("businessTypes$list").subscribe((res : string[]) => {
    //   for (let i = 0; i < res.length; i++) {
    //     this.businessTypes[i].formatted = res[i];
    //   }
    // });
    this.api.settingsData().subscribe(
      (response : any) => {
        const res = response._body._d;
        this.accountForm.controls["email"].setValue(res._a.email);
        this.franchiseForm.controls["name"].setValue(res._f.name);
        this.franchiseForm.controls["owner"].setValue(res._f.owner);
        this.franchiseForm.controls["website"].setValue(res._f.website);
        this.legal_valid = res._le.valid;
        this.stripeLogin = res._le._login;
        this.stripe_source_tkn = res._le._stripe_source_tkn;
        this.fcode = res._m.fc;
        this.fcodeImage = res._m.url;
        this.managerCode = res._m.mc;
        this.legal_place = res._le.line;
        this.locations = res._l;
        this.privacyForm.controls["notif"].setValue(res._p.notif);
        this.accountEmail = res._a.email;
        this.loaderService.handleLoader(false);
      }
    );
  }

  resetLocation() {
    this.franchise_place = null;
    this.locationForm.controls["lat"].setValue(null);
    this.locationForm.controls["lng"].setValue(null);
    this.locationForm.controls["postal"].setValue(null);
    this.locationForm.controls["address"].setValue(null);
    this.locationForm.controls["address"].setErrors({"invalidaddress" : true});

    this.locationForm.controls["address"].setValidators(Validators.required);
    this.locationForm.controls["address"].updateValueAndValidity();
    this.locationForm.controls["lat"].setValidators(Validators.required);
    this.locationForm.controls["lat"].updateValueAndValidity();
    this.locationForm.controls["lng"].setValidators(Validators.required);
    this.locationForm.controls["lng"].updateValueAndValidity();
    this.locationForm.controls["postal"].setValidators(Validators.required);
    this.locationForm.controls["postal"].updateValueAndValidity();

    this.startAutocomplete();
  }

  startAutocomplete() {
    let autoCompleteModal = this.modalController.create(AutoCompleteLocationComponent);
    autoCompleteModal.onDidDismiss(place => {
      if (!place) return;
      this.locationForm.controls["address"].setErrors(null);
      this.locationForm.controls["address"].setValue(place.formatted_address);
      this.locationForm.controls["lat"].setValue(place.geometry.location.lat());
      this.locationForm.controls["lng"].setValue(place.geometry.location.lng());
      this.locationForm.controls["postal"].setValue(place.address_components.filter(component => component.types.includes("postal_code"))[0].long_name);
      this.franchise_place = place;
    });
    autoCompleteModal.present();
  }

  togglePassword(hide : boolean) {
    this.passwordDisplay = hide ? 'password' : 'text';
  }

  validateDeletion() {
    const valid = this.deleteAccountForm.value.email.toLowerCase() === this.accountEmail;
    this.deletionIsValid = valid;
  }

  deleteLocation(index) {
    this.locations[index].d = true;
    this.franchise_place = null;
    this.locationForm.markAsDirty();
    this.locationForm.controls["address"].setValue(null)
    this.locationForm.controls["lat"].setValue(null);
    this.locationForm.controls["lng"].setValue(null);
    this.locationForm.controls["postal"].setValue(null);
    this.locationForm.controls["address"].setErrors(null);

    this.locationForm.controls["address"].clearValidators();
    this.locationForm.controls["address"].updateValueAndValidity();
    this.locationForm.controls["lat"].clearValidators();
    this.locationForm.controls["lat"].updateValueAndValidity();
    this.locationForm.controls["lng"].clearValidators();
    this.locationForm.controls["lng"].updateValueAndValidity();
    this.locationForm.controls["postal"].clearValidators();
    this.locationForm.controls["postal"].updateValueAndValidity();
  }

  onAccountSubmit() {
    if (this.accountForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateAccount(this.accountForm.value).subscribe(
      (response : any) => {
        const res = response._body;

        if (res._m === "_emailinuse") {
          this.snackbarService.handleError({"message" : "email_inuse", "isError" : true});
          this.accountForm.controls.email.setErrors({'inuse':true});
        } else if (res._m === "_passshort") {
          this.snackbarService.handleError({"message" : "pass_len_error", "isError" : true});
          this.accountForm.controls.email.setErrors({'tooshort':true});
        } else {
          this.snackbarService.handleError({"message" : "account_updated", "isError" : false})
          this.accountForm.reset(this.accountForm.value);
        }
        this.loaderService.handleLoader(false);
      }
    );
  }

  onFranchiseSubmit() {
    if (this.franchiseForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateFranchise(this.franchiseForm.value).subscribe(
      () => {
        this.snackbarService.handleError({ "message": "franchise_updated", "isError": false });
        this.franchiseForm.reset(this.franchiseForm.value);
        this.loaderService.handleLoader(false);
      }
    );
  }

  onLocationSubmit() {
    this.locationForm.controls.previous.setValue(this.locations);
    if (this.locationForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updateLocation(this.locationForm.value).subscribe(
      (response : any) => {
        const res = response._body;

        this.locations = res._data.l;

        this.snackbarService.handleError({"message" : "location_updated", "isError" : false})
        this.locationForm.reset();
        this.locationForm.controls["auth"].setValue(this.authService.getAUTH());
        this.locationForm.controls["address"].setValue(null)
        this.locationForm.controls["lat"].setValue(null);
        this.locationForm.controls["lng"].setValue(null);
        this.locationForm.controls["postal"].setValue(null);
        this.loaderService.handleLoader(false);
      }
    );
  }

  onManagerSubmit() {
    this.loaderService.handleLoader(true);
    this.api.updateManagers(this.managerForm.value).subscribe(
      (res:any) => {
        this.snackbarService.handleError({ "message": "managers_updated", "isError": false });
        this.managerCode = res._body._d._c;
        this.loaderService.handleLoader(false);
      }
    );
  }

  onRedeem() {
    this.loaderService.handleLoader(true);
    this.api.redeemTab({auth : this.authService.getAUTH()}).subscribe(
      (response : any) => {
        const res = response._body;
        if (res._m == "_tabtoolow") {
          this.snackbarService.handleError({"message" : "low_tab", "isError" : true});
          this.loaderService.handleLoader(false);
          return;
        }

        if (res._m == "_invalidcc") {
          this.snackbarService.handleError({"message" : "invalid_card_details", "isError" : true});
          this.loaderService.handleLoader(false);
          return;
        }

        if (res._m == "_errorpayout") {
          this.snackbarService.handleError({"message" : "tab_payout_error", "isError" : true});
          this.loaderService.handleLoader(false);
          return;
        }

        this.snackbarService.handleError({"message" : "tab_redeemed", "isError" : false});
        this.loaderService.handleLoader(false);
      }
    );
  }

  onPrivacySubmit() {
    if (this.privacyForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.updatePrivacy(this.privacyForm.value).subscribe(
      () => {
        this.snackbarService.handleError({ "message": "privacy_updated", "isError": false });
        this.privacyForm.reset(this.privacyForm.value);
        this.loaderService.handleLoader(false);
      }
    );
  }

  onLegalView($event) {
    if ($event.length === 0) return;
    this.app.getRootNav().push(LegalViewerComponent, {type: $event});
    this.legalSelect.setValue(null);
  }

  getStripeUrl() {
    return `https://connect.stripe.com/express/oauth/authorize?redirect_uri=${process.env.STRIPE_REDIRECT_URI}&client_id=${process.env.STRIPE_CLIENT_ID}&state=${this.stripe_source_tkn}&suggested_capabilities[]=platform_payments`;
  }

  onStripeLogin() {
    const browser = this.iab.create(this.stripeLogin, "_blank", {
      toolbar: 'yes',
      location: 'no',
      toolbarposition: 'top',
      toolbarcolor: "#333333",
      toolbartranslucent: 'yes',
      closebuttoncolor: "#ffffff",
      navigationbuttoncolor: "#ffffff"
    });

    browser.on('exit').subscribe(() => {
      this.statusBar.overlaysWebView(true);
    });
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#333333');
    browser.show();
  }

  onStripeRegister() {
    const browser = this.iab.create(this.getStripeUrl(), "_blank", {
      toolbar: 'yes',
      location: 'no',
      toolbarposition: 'top',
      toolbarcolor: "#333333",
      toolbartranslucent: 'yes',
      closebuttoncolor: "#ffffff",
      navigationbuttoncolor: "#ffffff"
    });

    browser.on('exit').subscribe(() => {
      this.statusBar.overlaysWebView(true);
    });
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#333333');
    browser.show();
  }

  onDeleteSubmit() {
    if (this.deleteAccountForm.invalid) {
      return;
    }
    this.loaderService.handleLoader(true);
    this.api.deleteAccount(this.deleteAccountForm.value).subscribe(
      () => {
        this.snackbarService.handleError({ "message": "account_deleted", "isError": false });
        this.deleteAccountForm.reset(this.deleteAccountForm.value);
        this.loaderService.handleLoader(false);
        this.authService.logout();
      }
    );
  }
}
