import { Component, OnInit } from "@angular/core";
import { NavParams, AlertController } from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { APIService } from "../../app/auth/api.service";
import { TranslateService } from "@ngx-translate/core";
import { DomSanitizer, SafeResourceUrl, SafeHtml } from "@angular/platform-browser";

@Component({
  selector: "app-legal-viewer",
  templateUrl: './legal.viewer.component.html'
})
export class LegalViewerComponent implements OnInit {
  type: string;
  guidelines = '';
  dmca: SafeHtml = '';
  iframeSrc: SafeResourceUrl;
  legalDocTitle: string = '';
  legalDocs = [];

  constructor(private navParams: NavParams, private alertCtrl: AlertController, private sanitizer: DomSanitizer, private http: HttpClient, private api: APIService, private translateService: TranslateService) {
    this.translateService.get('legal_doc').subscribe((translation) => {
      this.legalDocTitle = translation;
    });
    this.translateService.get('legal$list').subscribe((translation) => {
      this.legalDocs = translation;
    });
  }

  ngOnInit() {
    this.type = this.navParams.get('type');
    this.switchDoc();
  }

  switchDoc() {
    this.http.get(this.api.api + '/translations/' + this.translateService.getDefaultLang() + '.guidelines.json')
      .subscribe((data: any) => {
          this.guidelines = data.business;
          this.dmca = this.sanitizer.bypassSecurityTrustHtml(data.dmca);
      });
    if (this.type === 'privacy-policy') {
      this.iframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl("https://app.termly.io/document/privacy-policy/968c9358-e990-4281-9c03-a281803ffd3c");
    } else if (this.type === 'disclaimer') {
      this.iframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl("https://app.termly.io/document/disclaimer/09668428-b7b4-47eb-8196-eac4199783af");
    } else if (this.type === 'terms-of-use') {
      this.iframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl("https://app.termly.io/document/terms-of-use-for-saas/749bc103-95f6-455e-bda7-7154b572ba0b");
    }
  }

  onDocSelect() {
    let prompt = this.alertCtrl.create({
    title: this.legalDocTitle,
    inputs : [
    {
        type:'radio',
        value: 'busiGuide',
        label: this.legalDocs[0]
    },
    {
        type:'radio',
        value:'privacy-policy',
        label: this.legalDocs[1]
    },
    {
        type:'radio',
        value:'terms-of-use',
        label: this.legalDocs[2]
    },
    {
        type:'radio',
        value:'dmca',
        label: this.legalDocs[3]
    },
    {
        type:'radio',
        value:'disclaimer',
        label: this.legalDocs[4]
    }],
    buttons : [
    {
        text: "OK",
        handler: data => {
          this.type = data;
          this.switchDoc();
        }
    }]});
    prompt.present();
  }
}
